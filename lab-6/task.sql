USE AdventureWorks2019;
GO

DROP PROCEDURE IF EXISTS dbo.WorkOrdersByMonths;
GO


-- Create stored procedure which using operator PIVOT
-- returns summary count of ordered products [Production].[WorkOrder].[OrderQty]
-- for given months [DueDate]. Data should be displayed for each year.
CREATE PROCEDURE dbo.WorkOrdersByMonths
	@ColumnsList NVARCHAR(MAX)
AS
BEGIN
	DECLARE @Sql NVARCHAR(MAX) = N'
		SELECT Year,
			' + @ColumnsList + '
		FROM (
			SELECT 
				YEAR(DueDate) AS Year,
				FORMAT(DueDate,''MMMM'') AS Month,
				SUM(OrderQty) AS Quantity	
			FROM Production.WorkOrder
			GROUP BY YEAR(DueDate), FORMAT(DueDate,''MMMM'')
		) AS SourceTable
		PIVOT (
			SUM(Quantity) 
			FOR Month IN (' + @ColumnsList + ')
		) AS Pvt
		ORDER BY Pvt.Year;
	';
	PRINT @Sql;
	EXECUTE sp_executesql @Sql;
END;
GO
	
EXECUTE dbo.WorkOrdersByMonths '[January],[February],[March],[April],[May],[June]';
GO