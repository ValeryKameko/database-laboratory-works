USE LabDatabase1;
GO

DROP TABLE IF EXISTS #Address;
GO



-- Add into [dbo].[Address]:
-- * [AccountNumber] NVARCHAR(15)
-- * [MaxPrice] MONEY
-- * computed [AccountID] = [AccountNumber] + 'ID'
ALTER TABLE dbo.Address
ADD 
	AccountNumber NVARCHAR(15),
	MaxPrice MONEY,
	AccountID AS CONCAT(AccountNumber, 'ID');

-- Check
--SELECT * FROM dbo.Address;
GO


-- Create temporary table #Address with PK [ID]
-- With all columns of [dbo].[Address] except [AccountID]
SELECT *
INTO #Address
FROM dbo.Address
WHERE 0 = 1;

ALTER TABLE #Address
DROP COLUMN AccountID;

ALTER TABLE #Address
ADD CONSTRAINT ID_PK PRIMARY KEY (ID);

-- Check
SELECT * FROM #Address;
GO


-- Fill #Address from [dbo].[Address]:
-- * [AccountNumber] = [Vendor].[AccountNumber]
-- * [MaxPrice] = max of [StandardPrice] from [Vendor]
WITH MaxPriceByVendor AS (
	SELECT Purchasing_ProductVendor.BusinessEntityID, MAX(Purchasing_ProductVendor.StandardPrice) AS MaxPrice
	FROM AdventureWorks2019.Purchasing.ProductVendor AS Purchasing_ProductVendor
	GROUP BY Purchasing_ProductVendor.BusinessEntityID
)
INSERT INTO #Address (AddressID, AddressLine1, AddressLine2, City, StateProvinceID, PostalCode, ModifiedDate, AccountNumber, MaxPrice)
SELECT
	Person_Address.AddressID,
	Person_Address.AddressLine1,
	COALESCE(Person_Address.AddressLine2, 'Unknown'),
	Person_Address.City,
	Person_Address.StateProvinceID,
	Person_Address.PostalCode,
	Person_Address.ModifiedDate,
	Purchasing_Vendor.AccountNumber,
	MaxPriceByVendor.MaxPrice
FROM AdventureWorks2019.Person.Address AS Person_Address
JOIN AdventureWorks2019.Person.BusinessEntityAddress AS Person_BusinessEntityAddress
	ON Person_Address.AddressID = Person_BusinessEntityAddress.AddressID
JOIN AdventureWorks2019.Purchasing.Vendor AS Purchasing_Vendor
	ON Person_BusinessEntityAddress.BusinessEntityID = Purchasing_Vendor.BusinessEntityID
JOIN MaxPriceByVendor
	ON MaxPriceByVendor.BusinessEntityID = Purchasing_Vendor.BusinessEntityID;

-- Check
-- SELECT * FROM #Address;
GO


-- Delete from #Address entry with ID = 293
DELETE FROM #Address
WHERE ID = 293;
GO


-- Merge into [dbo].[Address] table #Address fields [AccountNumber] and [MaxPrice]
-- link by [ID]:
-- * source, target -> update in target
-- * source, not target -> add to target
-- * not source, target -> update in target
MERGE INTO dbo.Address AS Target
USING #Address AS Source
	ON Target.ID = Source.ID
WHEN MATCHED THEN
	UPDATE SET 
		Target.AccountNumber = Source.AccountNumber,
		Target.MaxPrice = Source.MaxPrice
WHEN NOT MATCHED BY TARGET THEN
	INSERT (AddressID, AddressLine1, AddressLine2, City, StateProvinceID, PostalCode, ModifiedDate, AccountNumber, MaxPrice) 
	VALUES (AddressID, AddressLine1, AddressLine2, City, StateProvinceID, PostalCode, ModifiedDate, AccountNumber, MaxPrice)
WHEN NOT MATCHED BY SOURCE THEN
	DELETE;

-- Check
--SELECT * FROM dbo.Address;
GO
