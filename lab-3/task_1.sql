USE LabDatabase1;
GO


-- Add NVARCHAR(100) [PersonName] into [dbo].[Address]
ALTER TABLE dbo.Address
ADD PersonName NVARCHAR(100);
GO


-- Create table variable [Address] and
-- fill with [dbo].[Address] data:
-- * [StateProvinceID] = 77
-- * [AddressLine2] = CountryRegionCode,StateProvince,City 
DECLARE @Address TABLE (
	AddressID INT NOT NULL,
	AddressLine1 NVARCHAR(60) NOT NULL,
	AddressLine2 NVARCHAR(60) NOT NULL,
	City NVARCHAR(30) NOT NULL,
	StateProvinceID INT NOT NULL,
	PostalCode NVARCHAR(15) NOT NULL,
	ModifiedDate DATETIME NOT NULL,
	ID INT NOT NULL,
	PersonName NVARCHAR(100) NULL,
	PRIMARY KEY (ID)
);

INSERT INTO @Address (AddressID, ID, AddressLine1, AddressLine2, City, StateProvinceID, PostalCode, ModifiedDate, PersonName)
SELECT
	Address.AddressID, 
	Address.ID,
	Address.AddressLine1,
	CONCAT(
		Person_CountryRegion.CountryRegionCode, ',', 
		Person_StateProvince.Name, ',', 
		Address.City),
	Address.City,
	Address.StateProvinceID,
	Address.PostalCode,
	Address.ModifiedDate,
	Address.PersonName
FROM dbo.Address as Address
JOIN AdventureWorks2019.Person.StateProvince as Person_StateProvince
	ON Address.StateProvinceID = Person_StateProvince.StateProvinceID
JOIN AdventureWorks2019.Person.CountryRegion as Person_CountryRegion
	ON Person_StateProvince.CountryRegionCode = Person_CountryRegion.CountryRegionCode
WHERE Address.StateProvinceID = 77;

-- Check
--SELECT * FROM @Address;


-- Update in [dbo].[Address]:
-- * [AddressLine2] from @Address
-- * [PersonName] from [Person].[Person] as FirstName + LastName
UPDATE dbo.Address
SET 
	AddressLine2 = Local_Address.AddressLine2,
	PersonName = CONCAT(Person_Person.FirstName, ' ', Person_Person.LastName)
FROM @Address as Local_Address
JOIN AdventureWorks2019.Person.BusinessEntityAddress as Person_BusinessEntityAddress
	ON Local_Address.AddressID = Person_BusinessEntityAddress.AddressID
JOIN AdventureWorks2019.Person.Person as Person_Person
	ON Person_BusinessEntityAddress.BusinessEntityID = Person_Person.BusinessEntityID
WHERE Local_Address.ID = Address.ID;

-- Check
--SELECT * FROM dbo.Address;
GO


-- Delete from [dbo].[Address] where:
-- * [AddressType].[Name] = 'Main Office'
DELETE Address
FROM dbo.Address AS Address
JOIN AdventureWorks2019.Person.BusinessEntityAddress as Person_BusinessEntityAddress
	ON Address.AddressID = Person_BusinessEntityAddress.AddressID
JOIN AdventureWorks2019.Person.AddressType as Person_AddressType
	ON Person_BusinessEntityAddress.AddressTypeID = Person_AddressType.AddressTypeID
WHERE Person_AddressType.Name = 'Main Office';

--Check
--SELECT * FROM dbo.Address;
GO


-- Delete from [dbo].[Address] column [PersonName]
-- And drop custom constraints including default
ALTER TABLE dbo.Address
DROP COLUMN PersonName;

DECLARE @sql NVARCHAR(MAX) = N'';

SELECT @sql += N'
	ALTER TABLE ' + TABLE_SCHEMA + '.' + TABLE_NAME + '
	DROP CONSTRAINT ' + CONSTRAINT_NAME + ';'
FROM LabDatabase1.INFORMATION_SCHEMA.CONSTRAINT_TABLE_USAGE
WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'Address';

SELECT @sql += N'
	ALTER TABLE ' + Schemas.name + '.' + Tables.name + '
	DROP CONSTRAINT ' + DefaultConstraints.name + ';'
FROM sys.all_columns AS AllColumns
JOIN sys.tables AS Tables
	ON AllColumns.object_id = Tables.object_id
JOIN sys.schemas AS Schemas
	ON Tables.schema_id = Schemas.schema_id
JOIN sys.default_constraints AS DefaultConstraints
	ON AllColumns.default_object_id = DefaultConstraints.object_id
WHERE Schemas.name = 'dbo' AND Tables.Name = 'Address';

EXEC sp_executesql @sql;
GO


-- Drop table [dbo].[Address]
DROP TABLE dbo.Address;
GO