USE AdventureWorks2019;
GO

DROP PROCEDURE IF EXISTS dbo.Deserialize_XmlAddresses;
GO


-- Create stored procedure returning table from serialized XML
CREATE PROCEDURE dbo.Deserialize_XmlAddresses
	@XmlAddresses XML
AS 
BEGIN
	SELECT 
		XmlAddress.value('./@ID', 'INT') AS AddressID,
		XmlAddress.value('./City[1]', 'NVARCHAR(30)') AS City,
		XmlAddress.value('./Province[1]/@ID', 'INT') AS StateProvinceID,
		XmlAddress.value('./Province[1]/Region[1]', 'NVARCHAR(3)') AS CountryRegionCode
	FROM @XmlAddresses.nodes('Addresses/Address') AS XmlAddresses(XmlAddress);
END;
GO


-- Represent values of [AddressID], [City] from table [Person].[Address] and
-- columns [StateProvinceID], [CountryRegionCode] from table [Person].[StateProvince] in XML variable
DECLARE @Xml XML = (
	SELECT
		Address.AddressID AS "@ID",
		Address.City AS "City",
		StateProvince.StateProvinceID AS "Province/@ID",
		StateProvince.CountryRegionCode AS "Province/Region"
	FROM Person.Address AS Address
	JOIN Person.StateProvince AS StateProvince
		ON Address.StateProvinceID = StateProvince.StateProvinceID
	FOR XML	PATH('Address'), ROOT('Addresses')
);

SELECT @Xml;

EXECUTE dbo.Deserialize_XmlAddresses @Xml;
GO
