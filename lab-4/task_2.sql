USE AdventureWorks2019;
GO

DROP INDEX IF EXISTS CountryRegion_SalesTerritory_View_Idx
	ON CountryRegion_SalesTerritory_View;
DROP VIEW IF EXISTS CountryRegion_SalesTerritory_View;
GO


-- Create VIEW which represents data from tables
-- [Person].[CountryRegion] and [Sales].[SalesTerritory]
-- Create unique clustered index on [TerritoryID]
CREATE VIEW CountryRegion_SalesTerritory_View 
WITH SCHEMABINDING
AS 
SELECT 
	Sales_SalesTerritory.TerritoryID,
	Sales_SalesTerritory.Name AS SalesTerritory_Name,
	Sales_SalesTerritory.[Group],
	Sales_SalesTerritory.SalesYTD,
	Sales_SalesTerritory.SalesLastYear,
	Sales_SalesTerritory.CostYTD,
	Sales_SalesTerritory.CostLastYear,
	Sales_SalesTerritory.ModifiedDate AS SalesTerritory_ModifiedDate,
	Person_CountryRegion.CountryRegionCode,
	Person_CountryRegion.Name AS CountryRegion_Name,
	Person_CountryRegion.ModifiedDate AS CountryRegion_ModifiedDate
FROM Sales.SalesTerritory AS Sales_SalesTerritory
JOIN Person.CountryRegion AS Person_CountryRegion
	ON Sales_SalesTerritory.CountryRegionCode = Person_CountryRegion.CountryRegionCode;
GO

CREATE UNIQUE CLUSTERED INDEX CountryRegion_SalesTerritory_View_Idx
ON CountryRegion_SalesTerritory_View(TerritoryID);
GO


-- Create INSTEAD OF trigger for INSERT, DELETE and UPDATE
-- which changes [Person].[CountryRegion] and [Sales].[SalesTerritory] accordingluy
CREATE TRIGGER CountryRegion_SalesTerritory_View_INSERT
ON CountryRegion_SalesTerritory_View
INSTEAD OF INSERT
AS
BEGIN
	INSERT INTO Person.CountryRegion (CountryRegionCode, Name, ModifiedDate)
	SELECT DISTINCT CountryRegionCode, CountryRegion_Name, CountryRegion_ModifiedDate
	FROM INSERTED AS Inserted
	WHERE NOT EXISTS (
		SELECT 1 
		FROM Person.CountryRegion AS Person_CountryRegion 
		WHERE Inserted.CountryRegionCode = Person_CountryRegion.CountryRegionCode
	);
	
	INSERT INTO Sales.SalesTerritory (
		Name,
		CountryRegionCode,
		[Group],
		SalesYTD,
		SalesLastYear,
		CostYTD,
		CostLastYear,
		ModifiedDate
	)
	SELECT 
		SalesTerritory_Name,
		CountryRegionCode,
		[Group],
		SalesYTD,
		SalesLastYear,
		CostYTD,
		CostLastYear,
		SalesTerritory_ModifiedDate
	FROM INSERTED;
END;
GO

CREATE TRIGGER CountryRegion_SalesTerritory_View_UPDATE
ON CountryRegion_SalesTerritory_View
INSTEAD OF UPDATE
AS
BEGIN
	MERGE INTO Person.CountryRegion AS Target
	USING INSERTED AS Source
		ON Target.CountryRegionCode = Source.CountryRegionCode
	WHEN MATCHED THEN
		UPDATE SET 
			Target.Name = Source.CountryRegion_Name,
			Target.ModifiedDate = Source.CountryRegion_ModifiedDate
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (CountryRegionCode, Name, ModifiedDate)
		VALUES (CountryRegionCode, CountryRegion_Name, CountryRegion_ModifiedDate);

	MERGE INTO Sales.SalesTerritory AS Target
	USING INSERTED AS Source
		ON Target.TerritoryID = Source.TerritoryID
	WHEN MATCHED THEN
		UPDATE SET 
			Target.Name = Source.SalesTerritory_Name,
			Target.CountryRegionCode = Source.CountryRegionCode,
			Target.[Group] = Source.[Group],
			Target.SalesYTD = Source.SalesYTD,
			Target.SalesLastYear = Source.SalesLastYear,
			Target.CostYTD = Source.CostYTD,
			Target.ModifiedDate = Source.SalesTerritory_ModifiedDate
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (Name, CountryRegionCode, [Group], SalesYTD, SalesLastYear, CostYTD, ModifiedDate)
		VALUES (SalesTerritory_Name, CountryRegionCode, [Group], SalesYTD, SalesLastYear, CostYTD, SalesTerritory_ModifiedDate);
END;
GO

CREATE TRIGGER CountryRegion_SalesTerritory_View_DELETE
ON CountryRegion_SalesTerritory_View
INSTEAD OF DELETE
AS
BEGIN
	DELETE Sales_SalesTerritory
	FROM Sales.SalesTerritory AS Sales_SalesTerritory
	JOIN DELETED AS Deleted
		ON Sales_SalesTerritory.TerritoryID = Deleted.TerritoryID;
	
	DELETE Person_CountryRegion
	FROM Person.CountryRegion AS Person_CountryRegion
	JOIN DELETED AS Deleted
		ON Person_CountryRegion.CountryRegionCode = Deleted.CountryRegionCode;
END;
GO


-- INSERT, UPDATE and DELETE from [CountryRegion_SalesTerritory_View]
INSERT INTO CountryRegion_SalesTerritory_View (
	SalesTerritory_Name,
	[Group],
	SalesYTD,
	SalesLastYear,
	CostYTD,
	CostLastYear,
	SalesTerritory_ModifiedDate,
	CountryRegionCode,
	CountryRegion_Name,
	CountryRegion_ModifiedDate
)
VALUES (
	'New Sales Territory Name',
	'New Sales Territory Group',
	199.99,
	59999.99,
	599.99,
	109999.99,
	GETDATE(),
	'COD',
	'New Code',
	GETDATE()
), (
	'Brand New Sales Territory Name',
	'Brand New Sales Territory Group',
	199.99,
	1999.99,
	299.99,
	4999.99,
	getdate(),
	'cod',
	'New code',
	getdate()
), (
	'Specially New Sales Territory Name',
	'Specially New Sales Territory Group',
	199.99,
	29999.99,
	399.99,
	49999.99,
	getdate(),
	'cot',
	'New Cote',
	getdate()
);
-- Check 
SELECT * FROM CountryRegion_SalesTerritory_View WHERE SalesTerritory_Name IN ('New Sales Territory Name', 'Specially New Sales Territory Name', 'Brand New Sales Territory Name');
SELECT * FROM Person.CountryRegion WHERE Name IN ('New Code', 'New Cote');
SELECT * FROM Sales.SalesTerritory WHERE Name IN ('New Sales Territory Name', 'Specially New Sales Territory Name', 'Brand New Sales Territory Name');
GO

UPDATE CountryRegion_SalesTerritory_View
SET 
	SalesTerritory_Name = 'Updated Sales Territory Name',
	[Group] = 'Updated Sales Territory Group',
	SalesYTD = 200.00,
	SalesLastYear = 30000.00,
	CostYTD = 400.00,
	CostLastYear = 50000.00,
	SalesTerritory_ModifiedDate = GETDATE(),
	CountryRegionCode = 'UPD',
	CountryRegion_Name = 'Updated Code',
	CountryRegion_ModifiedDate = GETDATE()
WHERE SalesTerritory_Name = 'New Sales Territory Name';
-- Check 
SELECT * FROM CountryRegion_SalesTerritory_View WHERE SalesTerritory_Name IN ('New Sales Territory Name', 'Specially New Sales Territory Name', 'Brand New Sales Territory Name');
SELECT * FROM Person.CountryRegion WHERE Name IN ('New Code', 'New Cote');
SELECT * FROM Sales.SalesTerritory WHERE Name IN ('New Sales Territory Name', 'Specially New Sales Territory Name', 'Brand New Sales Territory Name');
GO

DELETE FROM CountryRegion_SalesTerritory_View
WHERE CountryRegionCode = 'COD';

DELETE FROM CountryRegion_SalesTerritory_View
WHERE CountryRegionCode = 'COT';
-- Check 
SELECT * FROM CountryRegion_SalesTerritory_View WHERE SalesTerritory_Name IN ('New Sales Territory Name', 'Specially New Sales Territory Name', 'Brand New Sales Territory Name');
SELECT * FROM Person.CountryRegion WHERE Name IN ('New Code', 'New Cote');
SELECT * FROM Sales.SalesTerritory WHERE Name IN ('New Sales Territory Name', 'Specially New Sales Territory Name', 'Brand New Sales Territory Name');
GO