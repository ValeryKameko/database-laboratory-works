USE AdventureWorks2019;
GO

DROP TABLE IF EXISTS Person.CountryRegionHst;
DROP TABLE IF EXISTS Person.CountryRegionHst;
DROP VIEW IF EXISTS Person.CountryRegion_View;
DROP TRIGGER IF EXISTS Person.CountryRegion_INSERT;
DROP TRIGGER IF EXISTS Person.CountryRegion_UPDATE;
DROP TRIGGER IF EXISTS Person.CountryRegion_DELETE;
GO

-- Create table [Person].[CountryRegionHst] to store changes in [Person].[CountryRegion]:
-- * [ID] -- PK IDENTITY(1, 1);
-- * [Action] -- Insert/Update/Delete
-- * [ModifiedDate] -- action date and time
-- * [SourceID] -- PK in changed table
-- * [UserName] -- user name of the person who changed
-- * ... any other if needed
CREATE TABLE Person.CountryRegionHst (
	ID INT IDENTITY(1, 1) NOT NULL,
	Action VARCHAR(16) NOT NULL,
	ModifiedDate DATETIME NOT NULL,
	SourceID NVARCHAR(3) NOT NULL,
	UserName VARCHAR(256) NOT NULL,
	SystemUserName VARCHAR(256) NOT NULL,

	CONSTRAINT PK_CountryRegionHst PRIMARY KEY (ID),
	CONSTRAINT Action_Enum CHECK (Action IN ('Insert', 'Update', 'Delete')),
);
GO


-- Create 3 AFTER triggers for INSERT, UPDATE, DELETE for [Person].[CountryRegion]
-- For any action insert related entry in [Person].[CountryRegionHst]
CREATE TRIGGER CountryRegion_INSERT
ON Person.CountryRegion
AFTER INSERT
AS 
INSERT INTO Person.CountryRegionHst (Action, ModifiedDate, SourceID, UserName, SystemUserName)
SELECT 'Insert', GETDATE(), CountryRegionCode, CURRENT_USER, SYSTEM_USER
FROM INSERTED;
GO

CREATE TRIGGER CountryRegion_UPDATE
ON Person.CountryRegion
AFTER UPDATE
AS 
INSERT INTO Person.CountryRegionHst (Action, ModifiedDate, SourceID, UserName, SystemUserName)
SELECT 'Update', GETDATE(), CountryRegionCode, CURRENT_USER, SYSTEM_USER
FROM INSERTED;
GO

CREATE TRIGGER CountryRegion_DELETE
ON Person.CountryRegion
AFTER DELETE
AS 
INSERT INTO Person.CountryRegionHst (Action, ModifiedDate, SourceID, UserName, SystemUserName)
SELECT 'Delete', GETDATE(), CountryRegionCode, CURRENT_USER, SYSTEM_USER
FROM DELETED;
GO


-- Create VIEW which representing all columns of [Person].[CountryRegion]
-- Make impossible to observe source code of VIEW
CREATE VIEW Person.CountryRegion_View
WITH ENCRYPTION
AS
SELECT * FROM Person.CountryRegion;
GO

-- Check
--SELECT TABLE_NAME, VIEW_DEFINITION
--FROM INFORMATION_SCHEMA.Views
--WHERE TABLE_NAME='CountryRegion_View';
--GO


-- Check that INSERT, UPDATE and DELETE works through VIEW
INSERT INTO Person.CountryRegion_View (CountryRegionCode, Name, ModifiedDate)
VALUES 
	('COD', 'CODE NAME', CAST('2015-09-20 12:30:00' AS DATETIME)),
	('COR', 'CORE NAME', CAST('2016-11-23 23:33:00' AS DATETIME)),
	('COP', 'COPE NAME', CAST('2022-10-03 06:14:00' AS DATETIME));

UPDATE Person.CountryRegion_View
SET Name = CONCAT('NEW ', Name)
WHERE CountryRegionCode IN ('COD', 'COP');

DELETE FROM Person.CountryRegion_View
WHERE CountryRegionCode IN ('COD', 'COR', 'COP');
GO

-- Check
SELECT * FROM Person.CountryRegionHst;
GO