USE AdventureWorks2019;
GO


-- Employees which send resumes
SELECT JobCandidate.BusinessEntityID AS BusinessEntityID,
       OrganizationLevel,
	   JobTitle,
	   JobCandidateID,
	   Resume
FROM HumanResources.Employee AS Employee
INNER JOIN HumanResources.JobCandidate AS JobCandidate
	ON Employee.BusinessEntityID = JobCandidate.BusinessEntityID;
GO


-- Departments with > 10 employees
SELECT Department.DepartmentID AS DepartmentID,
	   Department.Name AS Name,
	   COUNT(Employee.BusinessEntityID) AS EmpCount
FROM HumanResources.Employee AS Employee
JOIN HumanResources.EmployeeDepartmentHistory AS EmployeeDepartmentHistory
	ON Employee.BusinessEntityID = EmployeeDepartmentHistory.BusinessEntityID
JOIN HumanResources.Department AS Department
	ON EmployeeDepartmentHistory.DepartmentID = Department.DepartmentID
WHERE
	EmployeeDepartmentHistory.EndDate IS NULL
GROUP BY 
	Department.DepartmentID, 
    Department.Name
HAVING COUNT(Employee.BusinessEntityID) > 10;
GO


-- Aggregate sick hours ordered by hire date
SELECT Department.Name AS Name,
	   Employee.HireDate AS HireDate,
	   Employee.SickLeaveHours AS SickLeaveHours,
	   SUM(Employee_.SickLeaveHours) AS AccumulateSum
FROM HumanResources.EmployeeDepartmentHistory AS EmployeeDepartmentHistory
JOIN HumanResources.Employee AS Employee
	ON Employee.BusinessEntityID = EmployeeDepartmentHistory.BusinessEntityID
JOIN HumanResources.Department AS Department
	ON EmployeeDepartmentHistory.DepartmentID = Department.DepartmentID
JOIN HumanResources.EmployeeDepartmentHistory AS EmployeeDepartmentHistory_
	ON EmployeeDepartmentHistory_.DepartmentID = Department.DepartmentID
JOIN HumanResources.Employee AS Employee_
	ON Employee_.HireDate <= Employee.HireDate
	AND Employee_.BusinessEntityID = EmployeeDepartmentHistory_.BusinessEntityID
GROUP BY
	Department.Name,
	Employee.HireDate,
	Employee.SickLeaveHours
ORDER BY
	Department.Name ASC, 
	HireDate ASC;