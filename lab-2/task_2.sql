USE master;
GO

DROP DATABASE IF EXISTS LabDatabase1;
GO
 
CREATE DATABASE LabDatabase1;
GO

USE LabDatabase1;
GO


-- Create Address table (same as AdventureWorks2019.Person.Address
-- but without [SpatialLocation] and [rowguid])
CREATE TABLE dbo.Address(
	AddressID INT NOT NULL,
	AddressLine1 NVARCHAR(60) NOT NULL,
	AddressLine2 NVARCHAR(60) NULL,
	City NVARCHAR(30) NOT NULL,
	StateProvinceID INT NOT NULL,
	PostalCode NVARCHAR(15) NOT NULL,
	ModifiedDate DATETIME NOT NULL,
);
GO


-- Add INT [ID] field with initial 1 and increment 1, add UNIQUE constraint
ALTER TABLE dbo.Address 
ADD ID INT IDENTITY(1, 1) 
	CONSTRAINT ID_Unique UNIQUE;
GO


-- Add constraint to [StateProvinceID] to have only odd numbers
ALTER TABLE dbo.Address
ADD CONSTRAINT StateProvinceID_Odd
CHECK (StateProvinceID % 2 = 1);
GO


-- Add default value to [AddressLine2] field to 'Unknown'
ALTER TABLE dbo.Address
ADD CONSTRAINT AddressLine2_Default
DEFAULT 'Unknown' for AddressLine2;
GO


-- Insert data from AdventureWorks2019.Person.Address
-- * CountryRegion.Name = /^a*/
-- * StateProvinceID % 2 = 1
-- * AddressLine2 by default
INSERT INTO dbo.Address (
	AddressID,
	AddressLine1, 
	City, 
	StateProvinceID, 
	PostalCode, 
	ModifiedDate)
SELECT 
	Person_Address.AddressID,
	Person_Address.AddressLine1,
	Person_Address.City,
	Person_Address.StateProvinceID,
	Person_Address.PostalCode,
	Person_Address.ModifiedDate
FROM AdventureWorks2019.Person.Address AS Person_Address
JOIN AdventureWorks2019.Person.StateProvince AS Person_StateProvince
	ON Person_Address.StateProvinceID = Person_StateProvince.StateProvinceID
JOIN AdventureWorks2019.Person.CountryRegion AS Person_CountryRegion
	ON Person_StateProvince.CountryRegionCode = Person_CountryRegion.CountryRegionCode
WHERE 
	Person_CountryRegion.Name LIKE 'a%' AND 
	Person_Address.StateProvinceID % 2 = 1;
-- Check
-- SELECT * FROM dbo.Address;
GO


-- Alter [AddressLine2] to NOT NULL
ALTER TABLE dbo.Address
ALTER COLUMN AddressLine2 NVARCHAR(60) NOT NULL;
GO