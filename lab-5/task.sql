USE AdventureWorks2019;
GO

DROP FUNCTION IF EXISTS dbo.Calculate_ProductSubcategory_ProductCount;
DROP FUNCTION IF EXISTS dbo.FindProducts_WithinProductSubcategory_WithCostGreater1000;
DROP FUNCTION IF EXISTS dbo.FindProducts_WithinProductSubcategory_WithCostGreater1000_Old;
GO


-- Create scalar value function which: 
-- * accepts [ID] of [Production].[ProductSubcategory].[ProductSubcategoryID]
-- * returns count of [Production].[Product] within ProductSubcategory
CREATE FUNCTION dbo.Calculate_ProductSubcategory_ProductCount(@ID INT)
RETURNS BIGINT
AS
BEGIN
	DECLARE @ProductCount BIGINT;
	SET @ProductCount = (
		SELECT Count(Product.ProductID)
		FROM Production.Product
		WHERE ProductSubcategoryID = @ID
	);
	RETURN(@ProductCount);
END;
GO
-- Check
SELECT dbo.Calculate_ProductSubcategory_ProductCount(ProductCategoryID)
FROM Production.ProductSubcategory;
GO


-- Create inline table-valued function which:
-- * accepts [ID] of product subcategory with [Production].[ProductSubcategory].[ProductSubcategoryID]
-- * returns [Production].[Product] which belongs to given subcategory and has [StandardCost] > 1000
CREATE FUNCTION dbo.FindProducts_WithinProductSubcategory_WithCostGreater1000(@ID INT)
RETURNS TABLE
AS
RETURN
	SELECT *
	FROM Production.Product
	WHERE 
		ProductSubcategoryID = @ID AND 
		StandardCost > 1000;
GO


-- Call [dbo].[FindProducts_WithinProductSubcategory_WithCostGreater1000] for every product subcategory [Production].[ProductSubcategory]:
-- * using CROSS APPLY
-- * using OUTER APPLY
SELECT *
FROM Production.ProductSubcategory
CROSS APPLY dbo.FindProducts_WithinProductSubcategory_WithCostGreater1000(ProductSubcategoryID);

SELECT *
FROM Production.ProductSubcategory
OUTER APPLY dbo.FindProducts_WithinProductSubcategory_WithCostGreater1000(ProductSubcategoryID);
GO


-- Change [dbo].[FindLast2_Customer_SalesOrderHeaders] to multistatement version
-- Save previous implementation and test
EXEC sp_rename 
	'dbo.FindProducts_WithinProductSubcategory_WithCostGreater1000', 
	'FindProducts_WithinProductSubcategory_WithCostGreater1000_Old', 
	object
GO

CREATE FUNCTION dbo.FindProducts_WithinProductSubcategory_WithCostGreater1000(@ID INT)
RETURNS @ResultProducts TABLE (
	ProductID INT NOT NULL,
	Name dbo.Name NOT NULL,
	ProductNumber NVARCHAR(25) NOT NULL,
	MakeFlag dbo.Flag NOT NULL,
	FinishedGoodsFlag dbo.Flag NOT NULL,
	Color NVARCHAR(15) NULL,
	SafetyStockLevel SMALLINT NOT NULL,
	ReorderPoint SMALLINT NOT NULL,
	StandardCost MONEY NOT NULL,
	ListPrice MONEY NOT NULL,
	Size NVARCHAR(5) NULL,
	SizeUnitMeasureCode NCHAR(3) NULL,
	WeightUnitMeasureCode NCHAR(3) NULL,
	Weight DECIMAL(8, 2) NULL,
	DaysToManufacture INT NOT NULL,
	ProductLine NCHAR(2) NULL,
	Class NCHAR(2) NULL,
	Style NCHAR(2) NULL,
	ProductSubcategoryID INT NULL,
	ProductModelID INT NULL,
	SellStartDate DATETIME NOT NULL,
	SellEndDate DATETIME NULL,
	DiscontinuedDate DATETIME NULL,
	rowguid UNIQUEIDENTIFIER ROWGUIDCOL NOT NULL,
	ModifiedDate DATETIME NOT NULL
)
AS
BEGIN
	INSERT INTO @ResultProducts
	SELECT *
	FROM Production.Product
	WHERE 
		ProductSubcategoryID = @ID AND 
		StandardCost > 1000;
	RETURN
END;
GO

-- Check
SELECT *
FROM Production.ProductSubcategory
CROSS APPLY dbo.FindProducts_WithinProductSubcategory_WithCostGreater1000(ProductSubcategoryID);

SELECT *
FROM Production.ProductSubcategory
OUTER APPLY dbo.FindProducts_WithinProductSubcategory_WithCostGreater1000(ProductSubcategoryID);
GO
