DECLARE @sql NVARCHAR(MAX)
DECLARE @BackupFile NVARCHAR(max) = N'C:\\Samples\\AdventureWorks2019.bak';
DECLARE @DatabaseName NVARCHAR(max) = N'AdventureWorks2019';

SET @sql = N'RESTORE FILELISTONLY FROM DISK = @BackupFile;';
EXEC master.sys.sp_executesql @sql, N'@BackupFile NVARCHAR(MAX)', @BackupFile;

RETURN -- comment this to run backup


DECLARE @MDFLogicalName NVARCHAR(MAX) =	N'AdventureWorks2017'
DECLARE @LDFLogicalName NVARCHAR(MAX) =	N'AdventureWorks2017_log'
DECLARE @MDFFile NVARCHAR(MAX) = N'C:\\Program Files\\Microsoft SQL Server\\MSSQL15.SQLExpress\\MSSQL\\DATA\\AdventureWorks2019.mdf'
DECLARE @LDFFile NVARCHAR(MAX) = N'C:\\Program Files\\Microsoft SQL Server\\MSSQL15.SQLExpress\\MSSQL\\DATA\\AdventureWorks2019_log.ldf'

SET @sql = N'
	RESTORE DATABASE @DatabaseName
	FROM DISK = @BackupFile
	WITH REPLACE,
	MOVE @MDFLogicalName TO @MDFFile,
	MOVE @LDFLogicalName TO @LDFFile;
';
EXEC master.sys.sp_executesql @sql,
	N'
		@DatabaseName NVARCHAR(MAX),
		@BackupFile NVARCHAR(MAX),
		@MDFLogicalName NVARCHAR(MAX),
		@MDFFile NVARCHAR(MAX),
		@LDFLogicalName NVARCHAR(MAX),
		@LDFFile NVARCHAR(MAX)
	', 
	@DatabaseName,  @BackupFile, @MDFLogicalName, @MDFFile, @LDFLogicalName, @LDFFile;
GO

USE AdventureWorks2019 EXEC sp_changedbowner 'sa';