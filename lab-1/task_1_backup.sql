USE master;
GO

DECLARE @BackupToFile NVARCHAR(MAX) = N'C:\Samples\Lab1Database.bak';

BACKUP DATABASE Lab1Database 
TO DISK = @BackupToFile
WITH NAME = 'Lab 1 DB Backup';

DROP DATABASE Lab1Database;