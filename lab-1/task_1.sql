USE master;
GO

DROP DATABASE IF EXISTS Lab1Database;
GO

CREATE DATABASE Lab1Database;
GO

USE Lab1Database;
GO

CREATE SCHEMA sales;
GO 

CREATE TABLE sales.Orders (OrderNum INT NULL);