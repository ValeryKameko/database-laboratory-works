USE AdventureWorks2019;
GO


-- Single employees which born <= 1960
SELECT BusinessEntityID, 
	   BirthDate, 
	   MaritalStatus, 
	   Gender, 
	   HireDate
FROM HumanResources.Employee
WHERE 
	BirthDate <= '1960'
	AND MaritalStatus = 'S';
GO


-- Employees working 'Design Engineer' and sorted by hire date
SELECT BusinessEntityID, 
	   JobTitle, 
	   BirthDate, 
	   Gender, 
	   HireDate
FROM HumanResources.Employee
WHERE JobTitle = 'Design Engineer'
ORDER BY HireDate DESC;
GO


-- Employees working at 'Engineering' and working period
DECLARE @CurrentDate DATE = CAST(GETDATE() AS DATE)

SELECT Employee.BusinessEntityID as BusinessEntityID,
	   Department.DepartmentId as DepartmentId,
	   StartDate,
	   EndDate,
	   DATEDIFF(YEAR, StartDate, COALESCE(EndDate, @CurrentDate)) as YearsWorked
FROM HumanResources.Employee as Employee
JOIN HumanResources.EmployeeDepartmentHistory as EmployeeDepartmentHistory
	ON Employee.BusinessEntityID = EmployeeDepartmentHistory.BusinessEntityID
JOIN HumanResources.Department as Department
	ON EmployeeDepartmentHistory.DepartmentID = Department.DepartmentID
WHERE Department.Name = 'Engineering';
GO