USE master;
GO

DECLARE @BackupFromFile NVARCHAR(MAX) = N'C:\Samples\Lab1Database.bak';

RESTORE FILELISTONLY
FROM DISK = @BackupFromFile;

DECLARE @LDFLogicalName NVARCHAR(MAX) = N'Lab1Database',
		@MDFLogicalName NVARCHAR(MAX) = N'Lab1Database_log',
		@MDFPath NVARCHAR(MAX) = 'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\Lab1Database.mdf',
		@LDFPath NVARCHAR(MAX) = 'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\Lab1Database_log.ldf';

EXEC sp_executesql N'
	RESTORE DATABASE [Lab1Database]
	FROM DISK = @BackupFromFile
	WITH 
		MOVE @LDFLogicalName TO @LDFPath,
		MOVE @MDFLogicalName TO @MDFPath;
', N'
	@BackupFromFile NVARCHAR(MAX),
	@MDFLogicalName NVARCHAR(MAX),
	@MDFPath NVARCHAR(MAX),
	@LDFLogicalName NVARCHAR(MAX),
	@LDFPath NVARCHAR(MAX)
', @BackupFromFile, @MDFLogicalName, @MDFPath, @LDFLogicalName, @LDFPath;
